# Application Lifecycle Guide

## Objective

The objective of this document is to standardize how we develop applications and what is expected from developers and operations team to achieve production-grade quality.

This is not intended to be something immutable or to sacrifice the flexibility of your team.

Merge requests and discussions (Issues are a good place for this) are more than welcome!

## Stages

There are four stages that encompass what is expected from applications:

- **Development**
- **Release**
- **Monitoring**
- **Deprecation**

An application is expected at sometime to pass from all these stages.

### Development

- **Purpose definition**
- **Requirements gathering**
- **MVP definition**
- **Solution gathering and Architecture**
- **Proof of Concept (PoC) and Prototyping**

TODO: link to Hammock Driven Development

#### Purpose definition

An application is supposed to solve a problem, it must solve a very explicit problem and it must be very clear to all involved people what that problem is. 

A good way to create focus is to set a short and very specific statement that synthesizes what are you trying to solve.

What do you know about the problem?
What are your problem's constraints?
Are there any related problems?

Examples:

- *"Enable all employees to manage and analyze customers accounts"*
- *"Create a central place to access all $ORG services"*
- *"Convert Google spreadsheets into XML format"*

#### Requirements gathering

Once you have a clear understanding of what you are trying to solve, you have to dive deeper into your problem.

At this phase, you need to understand better what is the audience of your application, what are their needs and limitations. Without understanding your audience, you risk developing the wrong solution and waste time and resources.

Use this phase to gather all the information you can about the problem's subject. You can interview people from your target audience, talk to specialists on the subject and after all of that, create a list of desired features you can develop to solve the problem.

Always remember: **KEEP IT SIMPLE!**. Don't try to reach more than you need.

#### MVP definition

Now that you know what you need to create, it's time to **filter** from your requirements what the first stable version of application will look like.

At this phase, you can break big features into smaller ones, cut off (or delay) unecessary features, add features that emerge from selected ones (hidden dependencies and requirements) and simplify what you can.

The sooner you can bring value to your audience and get feedback, lesser will be the chance to develop something that is not useful.

#### Solution gathering and Architecture

> "Programmers know the benefits of everything and the tradeoffs of nothing"

:speech_balloon: Rich Hickey

Don't just propose a single solution. It's never a tradeoff analysis if you have only one alternative.
Take time to research and gather ways you can solve your problem.

Always think how your system architecture should be, always try to simplify how things are made.

- What are your data sources?
- How the components from your architecture will communicate with each other?
- How the data flows between the components?

After you gathered all this useful information, you need to decide which of the solutions is more suitable to your needs and constraints.
Don't forget to document all the alternatives and why you chose that one.

#### Proof of Concept (PoC) and Prototyping

Now it's time to validate your solution.

If your solution is something you don't have experience, create a PoC to verify that you are building the right thing and it's viable. 

If end users are going to interact with you application, create a prototype and test it with some people from your audience. Get feedback from users as soon as you can, it's much cheaper to validate your conceptions with prototypes rather than developing the wrong application.

Read more:
- [The Design Sprint](http://www.gv.com/sprint/)

---

### Development

<img src="https://gitlab.com/felipemocruha/application-lifecycle-guide/uploads/d249c3e8656d9897fbcbe81a9ad64638/architecture-cycle.png" width="500"/>

- **Prioritization**
- **Communication**
- **Clean Code**
- **Unit Tests**
- **Code Review**
- **Best Practices**
- **Documentation**
- **Automation**
- **Security**

#### Prioritization

There is always too much of things to do, more than you have time. Always keep in mind what brings more value and make that your first priority.

Don't fall into temptation to implement the most fun features and leave what really helps your user for later.

<img src="https://gitlab.com/felipemocruha/application-lifecycle-guide/uploads/6f64322d6e36aa307b729b558ee9665e/109-priorities.png" />

#### Communication

Find the most effective ways to communicate with your team and product stakeholders. It's very common during development phase the appearance of doubts and unexpected problems. If you can't communicate well and understand what needs to be done, your application won't succeed.

<img src="https://gitlab.com/felipemocruha/application-lifecycle-guide/uploads/bc2c6426145e05fa128b746eee96db76/dc_c110402.gif" />

#### Clean Code

Always try to write organized, self-documenting and maintainable code. Business requirements and technologies don't stay the same forever, remember that you (or someone else) will have to come back and understand the code you wrote.

Don't overcomplicate things, don't spend time building ~~leaky~~ abstractions that you don't need. Always try to keep it simple and solve the problem rather than proving how smart you are.

Take time to refactor and to reduce technical debt.

<img src="https://gitlab.com/felipemocruha/application-lifecycle-guide/uploads/be12282d0f74c865aedf4e35a7656d52/code_quality.png" />

#### Unit Tests

If your code is not tested, you don't have a clue if your application is behaving how it should.

Always write unit tests, use TDD ~~if~~ when you can.

#### Code Review

Only merge code into master branch after code review (or at least after pair programming).

Having someone else to review your code will help to guarantee you made good decisions, followed best practices, give you opportunity to improve your coding skills and help spreading knowledge between your peers.

Remember to create shorter merge requests and as often as possible, it's very hard to keep focus when you are reviewing too much changes on the codebase.

<img src="https://gitlab.com/felipemocruha/application-lifecycle-guide/uploads/792a93436605674623a987664259f74d/99-pull-request.png" />

#### Best Practices

Follow software development best pratices when suitable.

See:
  - [The Twelve-Factor App](https://12factor.net/)

<img src="https://gitlab.com/felipemocruha/application-lifecycle-guide/uploads/6f2fab830f84994e03b3856b54a86142/goto.png" />

#### Documentation

Your application should document:

- **Purpose and core concepts**
- **How to install dependencies**
- **What configurations it needs and how to set them**
- **How to setup for development**
- **How to run locally**
- **How build a deployment artifact**
- **How to run in production**
- **Any other useful information that helps a new developer onboarding**


#### Automation

Try to automate everything you don't have to think about to do. Makefiles, scripts and CI/CD pipelines are your best friends during development, use them as much as you can to improve developer experience.

Also, be careful to not automate very complex things that don't help you that much. Always remember this table:

<img src="https://gitlab.com/felipemocruha/application-lifecycle-guide/uploads/663cebd9d7cb12d83127373a355d3d78/is_it_worth_the_time.png" />

#### Security

Always keep security in mind when developing, taking shortcuts will cost much more later and potentially cause very bad consequences.

When using third-party libraries, make sure they are still supported and maintained. Check for vulnerability reports and issues on github. Bad choices will be much more difficult to replace once you wrote code using them.

---

### Release

<img src="https://gitlab.com/felipemocruha/application-lifecycle-guide/uploads/352aa24fe1e6ce44feda95b44878157d/cicd.png" width="600"/>

The release phase must involve CI/CD pipelines, they help to automate the process and enforce quality on every step. You can break releases in the following steps:  

- **Test**
- **Build**
- **Deploy**

Your application only can be considered "stable" if it complies with this checklist: TO DO

#### Test

You should only merge code into master branch if all tests have passed.

At this phase, you can run linters, unit tests, integration tests and anything else that helps your team to ensure the quality of the application being released.

#### Build

At this phase, assuming all your tests have passed, it's time to build all the artifacts your application need (binaries, bundles and docker images).

Make sure your application declares explicitly all its dependencies (e.g. requirements.txt, package.json, go.mod, et al...).

All applications and support services (e.g. databases and queues) must run on docker containers. Make sure to specify all environment dependencies on your Dockerfile.

Put your build steps on your project's CI/CD pipeline to generate all docker images with the same tag as your gitlab repository's release tag. Prefer [Semantic Versioning](https://semver.org/).

Read more:
- [Best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
- [7 best practices for building containers](https://cloud.google.com/blog/products/gcp/7-best-practices-for-building-containers)

#### Deploy

All deployments should run on our Kubernetes cluster.

At this phase, you will set all production configuration files for your application. Create [ConfigMaps](https://cloud.google.com/kubernetes-engine/docs/concepts/configmap) to non sensitive information, for credentials and protected information, create Kubernetes [Secrets](https://cloud.google.com/kubernetes-engine/docs/concepts/secret). For persistent data (e.g. databases disks), always create a [PersistentVolumeClaim](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims).

---

### Observability

> Observability is the activities that involve measuring, collecting, and analyzing various diagnostics signals from a system. These signals may include metrics, traces, logs, events, profiles and more.

:speech_balloon: *[Microservices Observability](https://medium.com/observability/microservices-observability-26a8b7056bb4)*

These are the important topics to keep in mind when we talk about observability:

- **Logging**
- **Metrics**
- **Visualization**
- **Alerting**

#### Logging

All of your application's logs should be treated as a stream to *standard output* (e.g. stdout or stderr).

On our cluster, we aggregate all logs that come from *standard output* and send to [Google Stackdriver](https://cloud.google.com/stackdriver), where it's much more easy to filter and query your application's logs.

#### Metrics

Your application should expose useful metrics to ensure it is behaving correctly and also business related metrics to help stakeholders monitor their goals are going on the expected direction.

Some useful metrics are:
- Number of errors
- Http request/response count
- Traffic latency
- Application's saturation

Use [Prometheus](https://prometheus.io/) to expose and export your metrics.

Read more:
- [An Appropriate Use of Metrics](https://martinfowler.com/articles/useOfMetrics.html)

#### Visualization

Create useful Grafana dashboards to monitor your application's custom metrics.

#### Alerting

TO DO

---

### Deprecation

TO DO

